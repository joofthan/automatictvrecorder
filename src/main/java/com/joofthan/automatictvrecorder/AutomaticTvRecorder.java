package com.joofthan.automatictvrecorder;

import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.model.Record;
import com.joofthan.automatictvrecorder.model.TvEntry;
import com.joofthan.automatictvrecorder.service.api.FileService;
import com.joofthan.automatictvrecorder.service.api.QualityService;
import com.joofthan.automatictvrecorder.service.api.RecorderService;
import com.joofthan.automatictvrecorder.service.api.TvGuideService;
import com.joofthan.automatictvrecorder.webbrowser.BrowserInstance;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AutomaticTvRecorder {

    private final TvGuideService magazine;
    private final RecorderService recorder;
    private final QualityService qualityService;
    private final FileService movieLibrary;

    public AutomaticTvRecorder(TvGuideService tvGuideService, RecorderService recorderService, QualityService qualityService, FileService fileService) {
        this.magazine = tvGuideService;
        this.recorder = recorderService;
        this.qualityService = qualityService;
        this.movieLibrary = fileService;
    }

    public void run() {
        BrowserInstance browser = new BrowserInstance();
        System.out.println("Loaded page: "+browser.favorites().joofthan().getTitle());
        browser.close();
        /*
        synchroniseAllWatchNextProvider();
        decideWhatToRecord();
        saveAllFinishedRecordsToLibrary();
         */
    }

    private void synchroniseAllWatchNextProvider() {
        //TODO v2: sync WatchNext-List from Apple, Google and Amazon
    }

    private void decideWhatToRecord() {

        List<TvEntry> availableMovies = magazine.listUpcomingMovies().stream()
                                        .filter(e -> !movieLibrary.containsRelatedFile(e))
                                        .sorted((Comparator.comparing(qualityService::getRating)))
                                        .collect(Collectors.toList());
        TvEntry movieOfTheWeek = availableMovies.get(availableMovies.size() - 1);

        recorder.addToRecordingQue(movieOfTheWeek);

    }

    private void saveAllFinishedRecordsToLibrary(){
        List<Record> list = recorder.listFinishedRecords();
        for (Record recording:list) {
            Media movieInfo = recording.getMedia();

            if(!movieLibrary.containsRelatedFile(movieInfo)){
                movieLibrary.addFile(recording);
            }
        }
    }

    public void shutdown(){
        this.magazine.shutdown();
        this.recorder.shutdown();
        this.qualityService.shutdown();
        this.movieLibrary.shutdown();
    }
}
