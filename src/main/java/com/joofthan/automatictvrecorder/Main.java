package com.joofthan.automatictvrecorder;

import com.joofthan.automatictvrecorder.service.KodiFileService;
import com.joofthan.automatictvrecorder.service.OmdbQuality;
import com.joofthan.automatictvrecorder.service.TvSpielfilmGuide;
import com.joofthan.automatictvrecorder.service.YouTvRecorder;
import com.joofthan.automatictvrecorder.service.api.FileService;
import com.joofthan.automatictvrecorder.service.api.QualityService;
import com.joofthan.automatictvrecorder.service.api.RecorderService;
import com.joofthan.automatictvrecorder.service.api.TvGuideService;
import com.joofthan.automatictvrecorder.util.Cache;
import com.joofthan.automatictvrecorder.util.CacheRam;
import com.joofthan.automatictvrecorder.util.NetworkUtil;
import com.joofthan.automatictvrecorder.webbrowser.BrowserInstance;

import java.io.IOException;
import java.util.Properties;

public class Main {

    private static String CONFIG_PATH = "config/";

    public static void main(String[] args){
        //Cache cache = new CacheFile(loadProperties(CONFIG_PATH + "cache.properties").getProperty("cachePath"));
        Cache cache = new CacheRam();
        NetworkUtil.setCache(cache);

        AutomaticTvRecorder automaticTvRecorder = setupAutomaticTvRecorder();
        automaticTvRecorder.run();
        automaticTvRecorder.shutdown();
        cache.persist();
    }

    private static AutomaticTvRecorder setupAutomaticTvRecorder(){
        BrowserInstance browser1 = new BrowserInstance();

        FileService kodiLibrary = new KodiFileService();
        QualityService omdbQuality = new OmdbQuality();
        RecorderService youTvRecorder = new YouTvRecorder(browser1, loadProperties(CONFIG_PATH + "youtv.properties"));
        TvGuideService tvSpielfilmMagazine = new TvSpielfilmGuide();

        return new AutomaticTvRecorder(tvSpielfilmMagazine, youTvRecorder, omdbQuality, kodiLibrary);
    }

    private static Properties loadProperties(String filename) {
        Properties p = new Properties();
        try {
            System.out.println("Loading File \""+filename+"\".");
            p.load(ClassLoader.getSystemResourceAsStream(filename));
        } catch (IOException | NullPointerException e) {
            //e.printStackTrace();
            System.err.println("Loading File \""+filename+"\" failed.");
            System.out.println("Using default values.");

            p.setProperty("cachePath", "user/cache/");
            p.setProperty("username","email@mail.de");
            p.setProperty("password", "password123");
            p.setProperty("recordingPath", "user/recordings");
            return p;
        }
        return p;
    }
}
