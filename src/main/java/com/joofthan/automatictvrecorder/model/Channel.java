package com.joofthan.automatictvrecorder.model;

public class Channel {

    public final String name;

    public Channel(String name) {
        this.name = name;
    }
}
