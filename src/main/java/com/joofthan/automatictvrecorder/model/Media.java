package com.joofthan.automatictvrecorder.model;

public class Media {
    public final String name;
    public final Integer year;
    
    public Media(String name, Integer year){
        this.name = name;
        this.year = year;
    }

    public boolean titleEquals(String name) {
        String trimmedThisName = this.name.replaceAll("[^A-Za-z0-9]","").trim().toUpperCase();
        String trimmedName = name.replaceAll("[^A-Za-z0-9]","").trim().toUpperCase();
        //int thisSize = trimmedThisName.length();
        //.substring(0,thisSize)

        if(trimmedThisName.equalsIgnoreCase(trimmedName)){
            return true;
        }

        return false;
    }
}
