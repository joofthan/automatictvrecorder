package com.joofthan.automatictvrecorder.model;

import java.io.File;

public class Record {
    private Media media;
    private File file;

    public Record(Media media, File file) {
        this.media = media;
        this.file = file;
    }

    public Media getMedia() {
        return media;
    }

    public File getFile() {
        return file;
    }
}
