package com.joofthan.automatictvrecorder.model;

import java.time.LocalDateTime;

public class TvEntry extends Media {
    public final Channel channel;
    public final LocalDateTime startTime;
    public final LocalDateTime endTime;

    public TvEntry(Channel channel, String name, Integer year, LocalDateTime startTime, LocalDateTime endTime) {
        super(name, year);
        this.channel = channel;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
