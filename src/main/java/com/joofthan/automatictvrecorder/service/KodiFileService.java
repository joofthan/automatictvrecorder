package com.joofthan.automatictvrecorder.service;

import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.model.Record;
import com.joofthan.automatictvrecorder.service.api.FileService;

import java.io.File;

public class KodiFileService implements FileService {
    @Override
    public File getFile(Media media) {
        return null;
    }

    @Override
    public void addFile(Record record) {
        saveToKodiLibrary(record);
    }

    private void saveToKodiLibrary(Record record) {

        Media mediaInfo = record.getMedia();
        File file = record.getFile();

        //TODO: https://gitlab.com/joofthan/automatictvrecorder/issues/4

        System.out.println("KodiFileService.saveToKodiLibrary is not Implemented: https://gitlab.com/joofthan/automatictvrecorder/issues/4");
    }

    @Override
    public void shutdown() {

    }
}
