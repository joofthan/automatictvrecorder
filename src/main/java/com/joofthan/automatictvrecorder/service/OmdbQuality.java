package com.joofthan.automatictvrecorder.service;

import com.joofthan.automatictvrecorder.service.api.QualityService;
import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.util.NetworkUtil;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class OmdbQuality implements QualityService {

    private static String API_URL ="http://www.omdbapi.com/?apikey=8f148f52&t=";

    @Override
    public short getRating(Media media) {
        return getImdbRating(media);
    }

    private short getImdbRating(Media media) {
        String content = "";
        try {
            String url = API_URL+URLEncoder.encode(media.name, "UTF-8")+"&y="+media.year;
            content  = NetworkUtil.retrivePage(url);
            if(content.contains("imdbRating")){
                String ratingString = content.split("imdbRating")[1].substring(3, 6);
                if("N/A".equals(ratingString)) return 0;
                short shortValue = (short) Math.round(Float.parseFloat(ratingString) * 10);
                return shortValue;
            }
        } catch (FileNotFoundException | NumberFormatException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private short getLatest(Media media){
        if (media.year == null) return 0;

        return media.year.shortValue();
    }

    @Override
    public void shutdown() {

    }
}
