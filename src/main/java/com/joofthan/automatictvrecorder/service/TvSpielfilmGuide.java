package com.joofthan.automatictvrecorder.service;

import com.joofthan.automatictvrecorder.service.api.TvGuideService;
import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.model.TvEntry;
import com.joofthan.automatictvrecorder.util.NetworkUtil;
import com.joofthan.automatictvrecorder.service.helper.tvspielfilm.TvSpielfilmParser;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class TvSpielfilmGuide implements TvGuideService {
    private static String JETZT_IM_TV = "https://www.tvspielfilm.de/tv-programm/sendungen/jetzt.html";
    private static String JETZT_IM_TV2 = "https://www.tvspielfilm.de/tv-programm/sendungen/?page=2&order=channel&freetv=1&cat[0]=SP&cat[1]=SE&cat[2]=RE&cat[3]=U&cat[4]=KIN&cat[5]=SPO&time=now&channel=";
    private static String JETZT_IM_TV3 = "https://www.tvspielfilm.de/tv-programm/sendungen/jetzt.html?page=2";
    private static String DEMNAECHST_IM_TV = "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=channel&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=shortly&channel=";

    /*neu*/
    private static String JETZT_IM_TV_HAUPTSENDER1 =            "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=now&channel=g%3A1";
    private static String JETZT_IM_TV_OEFFENTLICH1 =            "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=now&channel=g%3A2";
    private static String JETZT_IM_TV_SPARTENSENDER1 =          "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=now&channel=g%3A4103125";
    private static String JETZT_IM_TV_KINDER1 =                 "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=now&channel=g%3A10";

    private static String DEMNAECHST_IM_TV_HAUPTSENDER1 =       "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=shortly&channel=g%3A1";
    private static String DEMNAECHST_IM_TV_OEFFENTLICH1 =       "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=shortly&channel=g%3A2";
    private static String DEMNAECHST_IM_TV_SPARTENSENDER1 =     "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=shortly&channel=g%3A4103125";
    private static String DEMNAECHST_IM_TV_KINDER1 =            "https://www.tvspielfilm.de/tv-programm/sendungen/?page=1&order=time&freetv=1&cat[]=SP&cat[]=SE&cat[]=RE&cat[]=U&cat[]=KIN&cat[]=SPO&time=shortly&channel=g%3A10";

    private static String PRIME_TIME = "https://www.tvspielfilm.de/tv-programm/sendungen/abends.html";

    private static String PAGE_BASE = "https://www.tvspielfilm.de/tv-programm/sendungen/?page=";
    private static String THIS_WEEK_FILM_FILTER = "&order=time&date=thisWeek&freetv=1&cat[]=SP&time=day&channel=g%3A1";

    @Override
    public List<TvEntry> listUpcomingMovies() {
        List<TvEntry> list = new ArrayList<>();

        int pageNr = 1;
        while(true){
            try {
                List<TvEntry> result = getTvDataOverview(PAGE_BASE + pageNr + THIS_WEEK_FILM_FILTER);
                list.addAll(result);
                pageNr++;
            } catch (FileNotFoundException e) {
                break;
            }
        }

        return list;
    }

    @Override
    public TvEntry getTvEntry(Media media) {
        return listUpcomingMovies().stream().filter(f -> f.equals(media)).findFirst().orElse(null);
    }

    private List<TvEntry> getTvDataOverview(String url) throws FileNotFoundException {
        System.out.println("TvSpielfilmGuide: Download "+ url);
        String content = NetworkUtil.retrivePage(url);
        return TvSpielfilmParser.parsePageNowPages(content);
    }

    @Override
    public void shutdown() {

    }
}
