package com.joofthan.automatictvrecorder.service;

import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.model.Record;
import com.joofthan.automatictvrecorder.model.TvEntry;
import com.joofthan.automatictvrecorder.service.api.RecorderService;
import com.joofthan.automatictvrecorder.webbrowser.BrowserInstance;
import com.joofthan.automatictvrecorder.webbrowser.pages.youtv.YouTvDetails;
import com.joofthan.automatictvrecorder.webbrowser.pages.youtv.YouTvHome;
import com.joofthan.automatictvrecorder.webbrowser.pages.youtv.YouTvRecords;
import com.joofthan.automatictvrecorder.webbrowser.pages.youtv.YouTvSearch;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class YouTvRecorder implements RecorderService {
    private final String username;
    private final String password;
    private final String recordingPath;
    private final BrowserInstance browser;

    public YouTvRecorder(BrowserInstance browser, Properties properties){
        this.username = properties.getProperty("username");
        this.password = properties.getProperty("password");
        this.recordingPath = properties.getProperty("recordingPath");
        this.browser = browser;
    }

    @Override
    public void addToRecordingQue(TvEntry tvEntry) {
        YouTvHome homePage = browser.favorites().youTv();
        homePage.clickAcceptCookiesIfVisible();
        if(!homePage.isLoggedIn())homePage.login(username, password);
        YouTvSearch searchPage = homePage.search(tvEntry.name);

        boolean found = false;
        for(int s = 0; s < searchPage.seitenAnzahl(); s++) {
            searchPage.clickSeite(s);
            for (int i = 0; i < searchPage.zeilenAnzahl(); i++) {
                if (tvEntry.titleEquals(searchPage.zeile(i).getTitle())) {
                    if (!searchPage.zeile(i).isMarkedAsPending()) {
                        searchPage.zeile(i).clickRecord();
                        found = true;
                    }
                }
            }
        }

        if(!found) throw new RuntimeException("Adding \"" + tvEntry.name + "\" to RecordingQue was not successful.");
    }

    @Override
    public List<Record> listFinishedRecords() {
        ArrayList<Record> records = new ArrayList<>();

        YouTvHome homePage = browser.favorites().youTv();
        if(!homePage.isLoggedIn())homePage.login(username, password);
        YouTvRecords aufnahmen = homePage.clickMeineAufnahmen();
        for(int i = 0; i< aufnahmen.zeilenAnzahl(); i++){
            YouTvRecords.Row aktuelleZeile = aufnahmen.zeile(i);
            String title = aktuelleZeile.getTitle();
            YouTvDetails details = aktuelleZeile.clickTitle();
            int year = details.getYear();
            details.clickBack();
            String videoUrl = aufnahmen.zeile(i).clickPlay().getVideoUrl();
            File file = tryDownloadFinishedRecord(videoUrl);
            aufnahmen.clickMeineAufnahmen();

            records.add(new Record(new Media(title, year),file));
        }

        return records;
    }

    private File tryDownloadFinishedRecord(String videoUrl){
        try { return downloadFinishedRecord(videoUrl); } catch(IOException e) { return null; }
    }

    private File downloadFinishedRecord(String videoUrl) throws IOException {
        String fileName = videoUrl.substring(videoUrl.lastIndexOf("/") + 1);
        File file = new File(recordingPath + File.separator + fileName);

        System.out.println("YouTvRecorder: Start Download: " + videoUrl);
        InputStream in = new URL(videoUrl).openStream();
        try{
            Files.copy(in, file.toPath());
        }catch(FileAlreadyExistsException e){
            //Datei muss nicht erneut heruntergeladn werden, wenn sie bereits existiert.
        }
        in.close();
        System.out.println("YouTvRecorder: Download finished: " + file.getAbsoluteFile());

        return file;
    }

    @Override
    public void shutdown() {
        browser.close();
    }
}
