package com.joofthan.automatictvrecorder.service.api;

import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.model.Record;

import java.io.File;

public interface FileService extends BaseService {
    File getFile(Media media);

    default boolean containsRelatedFile(Media media){
        return getFile(media) != null;
    }

    void addFile(Record finishedRecord);
}
