package com.joofthan.automatictvrecorder.service.api;

import com.joofthan.automatictvrecorder.model.Media;

public interface QualityService extends BaseService {
    short getRating(Media media);
}
