package com.joofthan.automatictvrecorder.service.api;

import com.joofthan.automatictvrecorder.model.Record;
import com.joofthan.automatictvrecorder.model.TvEntry;

import java.util.List;

public interface RecorderService extends BaseService {
    void addToRecordingQue(TvEntry tvEntry);

    List<Record> listFinishedRecords();
}
