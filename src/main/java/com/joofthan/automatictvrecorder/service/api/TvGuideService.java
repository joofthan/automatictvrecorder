package com.joofthan.automatictvrecorder.service.api;

import com.joofthan.automatictvrecorder.model.Media;
import com.joofthan.automatictvrecorder.model.TvEntry;
import java.util.List;

public interface TvGuideService extends BaseService {
    List<TvEntry> listUpcomingMovies();
    TvEntry getTvEntry(Media media);
}
