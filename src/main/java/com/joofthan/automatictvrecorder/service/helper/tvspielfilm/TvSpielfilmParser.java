package com.joofthan.automatictvrecorder.service.helper.tvspielfilm;


import com.joofthan.automatictvrecorder.model.Channel;
import com.joofthan.automatictvrecorder.model.TvEntry;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TvSpielfilmParser {
    /**
     *
     * @param uhrzeit = "10:15 - 11:53"
     * @param datum = "So 30.06."
     * @return
     */
    public static LocalDateTime parseDate(String uhrzeit, String datum) {
        String datumkurz = datum.split(" ")[1];
        int hours = Integer.parseInt(uhrzeit.split(":")[0]);
        int min = Integer.parseInt(uhrzeit.split(":")[1].substring(0,2));

        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] s = datumkurz.split("\\.");
        int month = Integer.parseInt(datumkurz.split("\\.")[1]);
        int day = Integer.parseInt(datumkurz.split("\\.")[0]);

        return LocalDateTime.of(year,month,day,hours,min);
    }

    public static List<TvEntry> parsePageNowPages(String content){
        List<TvEntry> results = new ArrayList<>();
        try{
            String[] rows = content.split("info-table")[1].split("<tr class=\"hover\">");
            for (String row : rows) {
                if(row.contains("programm-col")){
                    String[] rawData = row.split(">");

                    String sendername = row.split("<td class=\"programm-col1\">")[1].split(" Programm\">")[0].split("title=\"")[1];
                    String datum = row.split(" <td class=\"col-2\">")[1].split("<span>")[1].split("</span>")[0];
                    String fullUhrzeit = row.split(" <td class=\"col-2\">")[1].split("<strong>")[1].split("</strong>")[0];
                    LocalDateTime startTime = TvSpielfilmParser.parseDate(fullUhrzeit.split(" - ")[0],datum);
                    LocalDateTime endTime = TvSpielfilmParser.parseDate(fullUhrzeit.split(" - ")[1],datum);
                    String urlZurDetailSeite = row.split(" <td class=\"col-3\">")[1].split("<strong><a href=\"")[1].split("\" ")[0];
                    String fullTitleWithYear = row.split(" <td class=\"col-3\">")[1].split("<a href=\"https://www.tvspielfilm.de")[1].split("title=\"")[1].split("\">")[0];
                    String genre = row.split(" <td class=\"col-4\">")[1].split("<span>")[1].split("</span>")[0];
                    String type = row.split(" <td class=\"col-5\">")[1].split("<span>")[1].split("</span>")[0].split(" <br/>")[0].trim();



                    /*
                    tvEntryModel.setSendername(sendername);
                    tvEntryModel.setStartTime(startTime);
                    tvEntryModel.setEndTime(endTime);
                    tvEntryModel.setTitel(titel);
                    tvEntryModel.setGenre(genre);
                    tvEntryModel.setType(type);
                    tvEntryModel.setUrlZurDetailSeite(urlZurDetailSeite);

                    if(tvEntryModel.getSendername().equals("RiC"))continue;
                    if(tvEntryModel.getSendername().equals("TOGGO plus"))continue;
                    if(tvEntryModel.getSendername().equals("DW (Europe)"))continue;
                    */

                    String title = fullTitleWithYear;
                    if(title.contains(",")){
                        title = title.substring(0, title.lastIndexOf(","));
                    }else{
                        title = title.substring(0, title.lastIndexOf(" "));
                    }
                    title = title.substring(0, title.lastIndexOf(" "));

                    Integer year = null;
                    try{
                        Integer.parseInt(fullTitleWithYear.substring(fullTitleWithYear.lastIndexOf(" ")+1));
                    }catch (NumberFormatException e){
                        System.out.println("There is no year in \"" + fullTitleWithYear + "\"");
                    }

                    if(!type.equals("Spielfilm")) continue;
                    results.add(new TvEntry(new Channel(sendername), title, year, startTime, endTime));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        //Collections.sort(results);
        return results;

    }

/*
    public static List<TvEntry> parseSearchPage(String content,String type){
        List<TvEntry> results = new ArrayList<>();
        try{
            DateFormat fmt = new SimpleDateFormat("dd. MMMM yyyy, HH:mm", Locale.GERMAN);
            String[] rows = content.split("primetime-table")[1].split("<tr>");
            String datum = "So 01.01.";
            for (String row : rows) {
                if(row.contains("<td colspan=\"5\" class=\"broadcastheader\">")){
                    //String dayName = row.split("TV-Sendungen am ")[1].substring(0,2);
                    //String day = row.split("TV-Sendungen am ")[1].split(", ")[0].split(".")[0];
                    //String month = row.split("TV-Sendungen am ")[1].split(". ")[1].split("</span>")[0];
                    datum = row.split("TV-Sendungen am ")[1].split(", ")[1].split("</span>")[0] +" " + Calendar.getInstance().get(Calendar.YEAR);
                }

                if(row.contains("<td class=\"col-1")){
                    String[] rawData = row.split(">");

                    String sendername = row.split("<td class=\"col-2\">")[1].split(" Programm\">")[0].split("title=\"")[1];
                    String fullUhrzeit = row.split(" <td class=\"col-1\">")[1].split("<span>")[1].split("</div>")[0].replace("</span>", "").split("\n")[0];


                    Date startTime = fmt.parse(datum + ", " + fullUhrzeit.split(" - ")[0]);
                    Date endTime = fmt.parse(datum + ", " + fullUhrzeit.split(" - ")[1]);
                    //Date startTime = TvSpielfilmParser.parseDate(fullUhrzeit.split(" - ")[0],datum);
                    //Date endTime = TvSpielfilmParser.parseDate(fullUhrzeit.split(" - ")[1],datum);

                    String urlZurDetailSeite = row.split(" <td class=\"col-3")[1].split("<h3><a href=\"")[1].split("\" ")[0];
                    String titel = row.split(" <td class=\"col-3")[1].split("<a href=\"https://www.tvspielfilm.de")[1].split("title=\"")[1].split("\">")[0];




                    TvEntryModel tvEntryModel = new TvEntryModel();
                    tvEntryModel.setUrlZurDetailSeite(urlZurDetailSeite);
                    tvEntryModel.setTitel(titel);
                    tvEntryModel.setGenre("");
                    tvEntryModel.setSendername(sendername);
                    tvEntryModel.setStartTime(startTime);
                    tvEntryModel.setEndTime(endTime);
                    tvEntryModel.setType(type);


                    Integer year = null;

                    results.add(new TvEntry(new Channel(sendername),titel,year,startTime,endTime));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        //Collections.sort(results);
        return results;
    }
    */
}
