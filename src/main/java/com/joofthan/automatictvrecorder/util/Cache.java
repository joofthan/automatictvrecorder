package com.joofthan.automatictvrecorder.util;

public interface Cache {
    void persist();

    boolean containsKey(String webPageUrl);

    String getValue(String webPageUrl);

    void save(String webPageUrl, String result);
}
