package com.joofthan.automatictvrecorder.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

public class CacheFile implements Cache {

    private Properties cache;
    private File cacheFile;

    public CacheFile(String cachePath) {
        cacheFile = new File(cachePath + "networkCache.properties");
        init();
    }

    public void save(String webPageUrl, String result) {
        cache.put(webPageUrl, result);
    }

    @Override
    public void persist(){
        try {
            cache.store(new FileOutputStream(cacheFile), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean containsKey(String webPageUrl) {
        return cache.containsKey(webPageUrl);
    }

    public String getValue(String webPageUrl) {
        return cache.getProperty(webPageUrl);
    }

    private void init() {
        if(cache != null) return;

        String today = String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
        cache = new Properties();
        try {
            boolean isCreated = cacheFile.createNewFile();
            if(isCreated){
                cache.put("version", today);
            }else{
                cache.load(new FileInputStream(cacheFile));
            }

            String version = cache.getProperty("version");
            if(!version.equals(today)){
                cache.clear();
                cache.put("version", today);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
