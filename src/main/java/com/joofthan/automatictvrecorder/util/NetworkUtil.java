package com.joofthan.automatictvrecorder.util;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class NetworkUtil {
    private static Cache cache;

    public static void setCache(Cache c){
        cache = c;
    }

    public static String retrivePage(String webPageUrl) throws FileNotFoundException {
        if(cache.containsKey(webPageUrl)) return cache.getValue(webPageUrl);

        String result = "";
        try{
            URL url = new URL(webPageUrl);
            URLConnection con = url.openConnection();
            InputStream inp = con.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inp));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            inp.close();

            result = builder.toString();

        }catch(FileNotFoundException e){
            throw e;
        }catch(IOException e){
            e.printStackTrace();
        }

        cache.save(webPageUrl, result);
        return result;
    }

}
