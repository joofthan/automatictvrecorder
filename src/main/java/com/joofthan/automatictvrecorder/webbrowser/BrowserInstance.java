package com.joofthan.automatictvrecorder.webbrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserInstance {
    private WebDriver driver;
    private Favorites favorites;

    WebDriver initWebDriver(String url){
        if(driver == null){
            try{
                driver = new ChromeDriver();
            }catch (Exception e){
                e.printStackTrace();
                driver = new FirefoxDriver();
            }
        }
        driver.get(url);

        return driver;
    }

    public Favorites favorites(){
        if(favorites == null){
            favorites = new Favorites(this);
        }

        return favorites;
    }

    public void close(){
        if(driver != null){
            driver.close();
            driver = null;
        }
    }
}
