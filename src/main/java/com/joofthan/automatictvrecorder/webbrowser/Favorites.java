package com.joofthan.automatictvrecorder.webbrowser;

import com.joofthan.automatictvrecorder.webbrowser.pages.joofthan.JoofthanHome;
import com.joofthan.automatictvrecorder.webbrowser.pages.youtv.YouTvHome;

public class Favorites {
    private final BrowserInstance browser;

    public Favorites(BrowserInstance browserInstance){
        this.browser = browserInstance;
    }

    public YouTvHome youTv(){
        return new YouTvHome(browser.initWebDriver("https://www.youtv.de/"));
    }

    public JoofthanHome joofthan(){
        return new JoofthanHome(browser.initWebDriver("https://www.youtv.de/"));
    }

}
