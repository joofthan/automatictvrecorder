package com.joofthan.automatictvrecorder.webbrowser.base;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class PageObject {

    protected WebDriver driver;

    public PageObject(WebDriver driver){
        this.driver = driver;
    }

    protected WebElement typeReplace(By byCondition, String text){
        //TODO: strg + a
        return type(byCondition, text);
    }

    protected WebElement type(By byCondition, String text){
        WebElement element = waitForElement(byCondition);
        element.sendKeys(text);

        return element;
    }

    protected void click(By byCondition){
        waitForElement(byCondition).click();
    }

    protected void pressEnterOn(By byCondition){
        WebElement element = waitForElement(byCondition);
        pressEnterOn(element);
    }

    protected void pressEnterOn(WebElement element){
        element.sendKeys(Keys.RETURN);
    }

    protected WebElement waitForElement(By byCondition){
        return waitForElement(byCondition, 10);
    }
    protected WebElement waitForElement(By byCondition, int timeOutInSeconds){
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        //WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(byCondition));

        return null;
    }

    protected boolean elementExists(By byCondition){
        try{
            waitForElement(byCondition, 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void waitSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
