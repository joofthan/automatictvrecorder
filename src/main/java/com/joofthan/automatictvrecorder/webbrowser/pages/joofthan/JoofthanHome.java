package com.joofthan.automatictvrecorder.webbrowser.pages.joofthan;

import com.joofthan.automatictvrecorder.webbrowser.base.PageObject;
import org.openqa.selenium.WebDriver;

public class JoofthanHome extends PageObject {
    public JoofthanHome(WebDriver driver) {
        super(driver);
    }

    public String getTitle(){
        return driver.getTitle();
    }
}
