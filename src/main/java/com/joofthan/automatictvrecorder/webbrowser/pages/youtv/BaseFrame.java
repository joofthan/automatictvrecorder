package com.joofthan.automatictvrecorder.webbrowser.pages.youtv;

import com.joofthan.automatictvrecorder.webbrowser.base.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public abstract class BaseFrame extends PageObject {

    public BaseFrame(WebDriver driver) {
        super(driver);
    }

    public void clickAcceptCookiesIfVisible(){
        if(elementExists(By.className("cookie-banner-buttons")));
        waitForElement(By.className("cookie-banner-buttons")).findElement(By.tagName("button")).click();
    }

    public void login(String username, String password){
        click(By.linkText("ANMELDEN"));
        type(By.id("session_email"), username);
        type(By.id("session_password"), password);
        pressEnterOn(By.id("session_password"));
        waitForElement(By.linkText("Impressum"));//Warten bis das Loginfenster weg ist.
    }

    public void clickTvProgram() {
        click(By.linkText("TV PROGRAMM"));
    }

    public BaseFrame clickDate(LocalDate day){
        String dayString = day.format(DateTimeFormatter.ofPattern("dd.MM"));
        List <WebElement> dayElements = waitForElement(By.className("tvguide-dates-wrapper")).findElements(By.tagName("li"));
        for(WebElement daySelector:dayElements){
            String daySelectorText = daySelector.findElement(By.tagName("small")).getText();
            if(daySelectorText.equals(dayString)){
                daySelector.click();
            }
        }
        waitSeconds(2);

        return this;
    }

    public YouTvTvProgram selectDateTime(LocalDateTime dateTime){
        String dayString = dateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        String timeString = dateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
        driver.get("https://www.youtv.de/tv-programm/hauptsender/" + dayString + "/" + timeString);
        return new YouTvTvProgram(driver);
    }


    public YouTvSearch search(String text){
        click(By.id("search_q"));
        typeReplace(By.id("search_q"), text);
        pressEnterOn(By.id("search_q"));

        return new YouTvSearch(driver);
    }

    public YouTvRecords clickMeineAufnahmen() {
        click(By.linkText("MEINE AUFNAHMEN"));

        return new YouTvRecords(driver);
    }

    public boolean isLoggedIn() {
        return !elementExists(By.linkText("ANMELDEN"));
    }
}
