package com.joofthan.automatictvrecorder.webbrowser.pages.youtv;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YouTvDetails extends BaseFrame{
    public YouTvDetails(WebDriver driver) {
        super(driver);
    }

    public Integer getYear(){
        String placeAndYear = waitForElement(By.className("broadcast-content")).findElement(By.tagName("b")).getText();
        String yearString = placeAndYear.substring(placeAndYear.indexOf("(")+1, placeAndYear.indexOf(")"));

        return Integer.valueOf(yearString);
    }

    public void clickBack(){
        waitForElement(By.className("broadcast-header")).findElement(By.className("close")).click();
        waitSeconds(1);
    }
}
