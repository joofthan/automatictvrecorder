package com.joofthan.automatictvrecorder.webbrowser.pages.youtv;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YouTvStream extends BaseFrame{
    public YouTvStream(WebDriver driver) {
        super(driver);
    }

    public String getVideoUrl() {
        String videoUrl = waitForElement(By.id("youtv-video")).getAttribute("src");

        return videoUrl;
    }
}
